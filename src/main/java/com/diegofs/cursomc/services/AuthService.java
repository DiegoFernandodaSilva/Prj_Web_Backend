package com.diegofs.cursomc.services;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.diegofs.cursomc.domain.Cliente;
import com.diegofs.cursomc.repositories.ClienteRepository;
import com.diegofs.cursomc.services.exceptions.ObjectNotFoundException;

@Service
public class AuthService {

	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private BCryptPasswordEncoder pe;
	
	
	Random randon = new Random();
	
	@Autowired
	EmailService emailService;
	
	public void sendNewPassword(String email) {
		 
		Cliente cliente = clienteRepository.findByEmail(email);
		
		if (cliente == null) {
			throw new ObjectNotFoundException("Email não encontrado");
		}
		
		String newPass = newPassword();
		cliente.setSenha(pe.encode(newPass));
		clienteRepository.save(cliente);
		emailService.sendNewPasswordEmail(cliente, newPass);
	 }

	private String newPassword() {
		char[] vet = new char[10];
		for (int i = 0; i < 10; i++) {
			vet[i] = randonChar();
		}
		return new String(vet);
	}

	private char randonChar() {
		int opt = randon.nextInt(3);
		if(opt == 0) { //Gera um Digito
			return (char) (randon.nextInt(10) + 49);
		}
		if(opt == 1) { //Gera Letra Maiuscula
			return (char) (randon.nextInt(26) + 65);
		}
		else { // Gera Letra Minuscula
			return (char) (randon.nextInt(26) + 97);
		}
	}
}
