package com.diegofs.cursomc.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.diegofs.cursomc.domain.Cidade;
import com.diegofs.cursomc.repositories.CidadeRepository;

@Service
public class CidadeService {

	@Autowired
	CidadeRepository repo;
	
	public List<Cidade> findCidades(Integer estadoId){
		return repo.findCidades(estadoId);
	}
}
