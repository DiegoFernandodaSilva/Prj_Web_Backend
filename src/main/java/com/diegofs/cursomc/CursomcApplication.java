package com.diegofs.cursomc;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.diegofs.cursomc.services.S3Service;

@SpringBootApplication
public class CursomcApplication implements CommandLineRunner{

	@Autowired
	S3Service s3Service;
	
	public static void main(String[] args) {
		SpringApplication.run(CursomcApplication.class, args);
	}

	@Override
	public void run(String... args) {
		//s3Service.uploadFile("C:\\Curso_Udemy\\imagens\\mini.jpg");
	}
}
